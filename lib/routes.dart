import 'package:estacionate_en_chillan/Modulos/Inicio/Inicio_view.dart';
import 'package:estacionate_en_chillan/Modulos/Menu/Menu_list_view.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> buildAppRoutes() {
  return {
    "/Menu": (BuildContext context) => new MenusPage(),
    "/Inicio": (BuildContext context) => new InicioPage(),

    /*"/PromocionItem":(BuildContext context) => new PromocionItemPage(),
    "/MenuItem":(BuildContext context) => new MenuItemPage(),*/
  };
}
