import 'package:estacionate_en_chillan/Modulos/Entidades/Estacionamiento_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AppState {
  FirebaseUser user;
  List<Estacionamiento> estacionamientos;

  AppState({@required this.user, @required this.estacionamientos});

  AppState.fromAppState(AppState another) {
    user = another.user;
    estacionamientos = another.estacionamientos;
  }

}
