import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:estacionate_en_chillan/Modulos/Estacionamientos/Estacionamiento_view.dart';
import 'package:estacionate_en_chillan/Modulos/Perfil/Perfil_view.dart';
import 'package:flutter/material.dart';

class MenusPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MenuList();
  }
}

class MenuList extends State<MenusPage> {
  int _currentIndex = 0;
  final databaseReference = Firestore.instance;



  List<BottomNavigationBarItem> containers_menu = [
    BottomNavigationBarItem(
        icon: Icon(Icons.search),
        backgroundColor: Colors.black,
        title: Text(
          "Buscar",
          style: TextStyle(fontFamily: "SenRegular"),
        )),
    BottomNavigationBarItem(
        icon: Icon(Icons.person),
        backgroundColor: Colors.black,
        title: Text("Mi Perfil", style: TextStyle(fontFamily: "SenRegular"))),
  ];

  List<Widget> _children = [
    Container(child: EstacionamientoScreen()),
    Container(child: PerfilScreen())
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  initState() {
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Más opciones", style: TextStyle(fontFamily: "SenRegular")),
        backgroundColor: Color.fromRGBO(0, 0, 0, 1),
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        // new
        currentIndex: _currentIndex,
        // this will be set when a new tab is tapped
        items: containers_menu,
      ),
      body: _children[_currentIndex],
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/Imagenes/fondo app.jpg"),
                  fit: BoxFit.cover)),
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                  child: Image.asset('assets/Imagenes/egipcialogogrande.png'),
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(0, 0, 0, 1),
                    image: DecorationImage(
                      image: NetworkImage(
                        'https://firebasestorage.googleapis.com/v0/b/egipciafestival-45700.appspot.com/o/piramides.jpg?alt=media&token=27ff8a9d-bfcb-4de9-acb6-461c62e70e08',
                      ),
                      fit: BoxFit.cover,
                    ),
                  )
                //color: Color.fromRGBO(238,153,61,1),
              ),
              ListTile(
                title: Text(
                  'Facebook',
                  style: TextStyle(fontFamily: 'Avenir', color: Colors.white),
                ),
                leading: CircleAvatar(
                  child: Image.asset('assets/Imagenes/facebook.png'),
                  backgroundColor: Colors.transparent,
                ),
                onTap: () async {
/*                    if (await canLaunch(
                      "https://www.facebook.com/Egipcia-Festival-108757777339739/")) {
                    await launch("https://www.facebook.com/Egipcia-Festival-108757777339739/");
                  }*/
                },
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.white,
                ),
              ),
              ListTile(
                title: Text(
                  'Instagram',
                  style: TextStyle(fontFamily: 'Avenir', color: Colors.white),
                ),
                leading: CircleAvatar(
                  child: Image.asset('assets/Imagenes/instagram.png'),
                  backgroundColor: Colors.transparent,
                ),
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.white,
                ),
                onTap: () async {
/*                    if (await canLaunch(
                      "https://www.instagram.com/egipciafestival/")) {
                    await launch(
                        "https://www.instagram.com/egipciafestival/");
                  }*/
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
