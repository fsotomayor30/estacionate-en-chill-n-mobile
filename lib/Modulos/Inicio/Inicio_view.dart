import 'package:estacionate_en_chillan/Modulos/Login/Login_view.dart';
import 'package:flutter/material.dart';

class InicioPage extends StatefulWidget {
  @override
  _InicioState createState() => _InicioState();
}

class _InicioState extends State<InicioPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Container(
          height: mediaQueryData.height,
          width: mediaQueryData.width,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Image.asset(
                "assets/Imagenes/icon_estacionateenchillan.png",
                width: mediaQueryData.width * 0.7,
              ),
              Container(
                width: mediaQueryData.width * .6,
                child: Column(
                  children: <Widget>[
                    LoginWidget(),
                    ButtonTheme(
                      minWidth: mediaQueryData.width * 0.6,
                      child: new RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(0.0),
                            side: BorderSide(color: Colors.transparent)),
                        padding: const EdgeInsets.all(8.0),
                        elevation: 0,
                        textColor: Colors.black,
                        color: Colors.white,
                        onPressed: () {
                          Navigator.of(context).popAndPushNamed('/Menu');
                        },
                        child: new Text("Iniciar Sesión más tarde",
                            style: TextStyle(fontFamily: "SenRegular")),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
