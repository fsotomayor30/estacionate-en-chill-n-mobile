import 'package:estacionate_en_chillan/Modulos/Login/actions/actions.dart';
import 'package:estacionate_en_chillan/app_state.dart';
import 'package:estacionate_en_chillan/services/firebase_auth_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PerfilScreen extends StatefulWidget {
  @override
  PerfilState createState() => PerfilState();
}

class PerfilState extends State<PerfilScreen> {
  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return (state.user != null)
              ? Scaffold(
                  bottomNavigationBar: Container(
                      width: mediaQueryData.width,
                      height: 50,
                      child: Column(children: <Widget>[
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              ButtonTheme(
                                minWidth: mediaQueryData.width * 0.6,
                                child: new RaisedButton(
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(0.0),
                                      side: BorderSide(color: Colors.black)),
                                  padding: const EdgeInsets.all(8.0),
                                  textColor: Colors.black,
                                  color: Colors.white,
                                  onPressed: () async {
                                    try {
                                      final auth = new FirebaseAuthService();
                                      await auth.signOutAccount();
                                      StoreProvider.of<AppState>(context)
                                          .dispatch(Usuario(null));
                                      Navigator.of(context)
                                          .popAndPushNamed('/Inicio');
                                    } catch (e) {
                                      print(e);
                                    }
                                  },
                                  child: new Text("Cerrar Sesión",
                                      style:
                                          TextStyle(fontFamily: "SenRegular")),
                                ),
                              ),
                            ])
                      ])),
                  body: StoreConnector<AppState, AppState>(
                    converter: (store) => store.state,
                    builder: (context, state) {
                      return (state.user != null)
                          ? Container(
                              height: mediaQueryData.height,
                              width: mediaQueryData.width,
                              color: Colors.white,
                              child: ListView(children: <Widget>[
                                ListTile(
                                  title: Container(
                                    height: 250,
                                    child: Stack(
                                      children: <Widget>[
                                        Image.asset(
                                          "assets/Imagenes/chillanbanner.jpg",
                                          width: mediaQueryData.width,
                                        ),
                                        Align(
                                            alignment: Alignment.bottomCenter,
                                            child: Image.network(
                                                state.user.photoUrl,
                                                width: 150,
                                                height: 150)),
                                      ],
                                    ),
                                  ),
                                ),
                                ListTile(
                                  title: Text(
                                    "Hola " + state.user.displayName,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 25, fontFamily: "SenBold"),
                                  ),
                                ),
                                Divider(
                                  height: 20,
                                ),
                                ListTile(
                                  title: Text(
                                    "Mis Simulaciones",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 20, fontFamily: "SenBold"),
                                  ),
                                ),
                                ListTile(
                                  title: Text(
                                    "Estacionamiento 1",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontFamily: "SenBold"),
                                  ),
                                  subtitle: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: <Widget>[
                                      Text(
                                        '15 minutos',
                                        style:
                                            TextStyle(fontFamily: "SenRegular"),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(5, 0, 5, 0),
                                        child: Icon(Icons.arrow_forward_ios),
                                      ),
                                      Text(
                                        '1.550 CLP',
                                        style:
                                            TextStyle(fontFamily: "SenRegular"),
                                      ),
                                    ],
                                  ),
                                ),
                                Divider(
                                  height: 20,
                                ),
                                ListTile(
                                  title: Text(
                                    "Mis Búsquedas",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 20, fontFamily: "SenBold"),
                                  ),
                                ),
                                ListTile(
                                    title: Text(
                                      "Estacionamiento 1",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontFamily: "SenBold"),
                                    ),
                                    subtitle: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: FaIcon(
                                              FontAwesomeIcons.mapMarkerAlt),
                                        ),
                                        Text(
                                          'Mall Arauco',
                                          style: TextStyle(
                                              fontFamily: "SenRegular"),
                                        ),
                                      ],
                                    )),
                                ListTile(
                                    title: Text(
                                      "Estacionamiento 2",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontFamily: "SenBold"),
                                    ),
                                    subtitle: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: FaIcon(
                                              FontAwesomeIcons.mapMarkerAlt),
                                        ),
                                        Text(
                                          'Mall Arauco',
                                          style: TextStyle(
                                              fontFamily: "SenRegular"),
                                        ),
                                      ],
                                    )),
                                Container(
                                  height: 20,
                                ),
                              ]))
                          : Container(
                              child: Text("test"),
                            );
                    },
                  ),
                )
              : Scaffold(
                  body: Center(
                    child: Container(
                      height: mediaQueryData.height,
                      width: mediaQueryData.width,
                      color: Colors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Image.asset(
                            "assets/Imagenes/icon_estacionateenchillan.png",
                            width: mediaQueryData.width * 0.7,
                          ),
                          Container(
                            width: mediaQueryData.width * .6,
                            child: Column(
                              children: <Widget>[
                                ButtonTheme(
                                  minWidth: mediaQueryData.width * 0.6,
                                  child: new RaisedButton(
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(0.0),
                                        side: BorderSide(color: Colors.black)),
                                    padding: const EdgeInsets.all(8.0),
                                    textColor: Colors.black,
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.of(context)
                                          .popAndPushNamed('/Inicio');
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Image.asset(
                                          "assets/Imagenes/left-arrow.png",
                                          width: 33,
                                        ),
                                        new Text("Ir a iniciar sesión",
                                            style: TextStyle(
                                                fontFamily: "SenRegular")),
                                      ],
                                    ),
                                  ),
                                ),
                                ButtonTheme(
                                  minWidth: mediaQueryData.width * 0.6,
                                  child: new RaisedButton(
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(0.0),
                                        side: BorderSide(
                                            color: Colors.transparent)),
                                    padding: const EdgeInsets.all(8.0),
                                    elevation: 0,
                                    textColor: Colors.black,
                                    color: Colors.white,
                                    onPressed: () {
                                      //TODO: Agregar acción
                                    },
                                    child: new Text("Necesitas iniciar sesión",
                                        style: TextStyle(
                                            fontFamily: "SenRegular")),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
        });
  }
}
