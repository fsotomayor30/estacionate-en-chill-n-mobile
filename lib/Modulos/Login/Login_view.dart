import 'package:estacionate_en_chillan/Modulos/Login/actions/actions.dart';
import 'package:estacionate_en_chillan/app_state.dart';
import 'package:estacionate_en_chillan/services/firebase_auth_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';

class LoginWidget extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginWidget> {
  bool loadLogin = false;

  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return ButtonTheme(
      minWidth: mediaQueryData.width * 0.6,
      child: new RaisedButton(
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(0.0),
            side: BorderSide(color: Colors.black)),
        padding: const EdgeInsets.all(8.0),
        textColor: Colors.black,
        color: Colors.white,
        onPressed: () async {
          try {
            setState(() {
              loadLogin = true;
            });
            final auth = new FirebaseAuthService();
            FirebaseUser res = await auth.singInWithGoogleAccount();
            StoreProvider.of<AppState>(context).dispatch(Usuario(res));
            Navigator.of(context).popAndPushNamed('/Menu');
          } catch (e) {
            print(e);
          }
        },
        child: (loadLogin)
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Loading(
                      indicator: BallPulseIndicator(),
                      size: 33.0,
                      color: Colors.black),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(
                      "Cargando",
                      style: TextStyle(
                        fontFamily: "SenRegular",
                      ),
                    ),
                  ),
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Image.asset(
                    "assets/Imagenes/iconGoogle.png",
                    width: 33,
                  ),
                  new Text("Iniciar Sesión",
                      style: TextStyle(fontFamily: "SenRegular")),
                ],
              ),
      ),
    );
  }
}
