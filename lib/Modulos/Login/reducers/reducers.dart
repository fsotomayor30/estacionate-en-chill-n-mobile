import 'package:estacionate_en_chillan/Modulos/Login/actions/actions.dart';
import 'package:estacionate_en_chillan/app_state.dart';

AppState reducer(AppState prevState, dynamic action) {
  AppState newState = AppState.fromAppState(prevState);

  if (action is Usuario) {
    newState.user = action.usuario;
  }
  return newState;
}
