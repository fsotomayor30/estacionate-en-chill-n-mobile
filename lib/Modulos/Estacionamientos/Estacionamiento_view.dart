import 'package:estacionate_en_chillan/Modulos/Estacionamientos/Detalle_destino_view.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:location/location.dart' as libreriaLocation;
import 'package:mapbox_search/mapbox_search.dart';

class EstacionamientoScreen extends StatefulWidget {
  @override
  EstacionamientoState createState() => EstacionamientoState();
}

class EstacionamientoState extends State<EstacionamientoScreen> {
  String apiKey =
      "pk.eyJ1IjoiZXN0YWNpb25hdGVlbmNoaWxsYW4iLCJhIjoiY2s4ZGltaW5mMHNkbzNsbnB1czhidmo1eCJ9.5y_8XqvC--TFuM92U_6N6Q";

  //SERVICIO DE UBICACIÓN
  static libreriaLocation.Location location = new libreriaLocation.Location();
  bool _serviceEnabled;
  libreriaLocation.PermissionStatus _permissionGranted;
  libreriaLocation.LocationData _locationData;

  //========================

  bool estadoServicio = true;
  bool estadoPermiso = true;

  final SearchBarController<MapBoxPlace> _searchBarController =
  SearchBarController();
  bool isReplay = false;

  @override
  initState() {
    super.initState();
    permisos();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future permisos() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        setState(() {
          estadoServicio = false;
        });
      } else {
        setState(() {
          estadoServicio = true;
        });
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == libreriaLocation.PermissionStatus.DENIED) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != libreriaLocation.PermissionStatus.GRANTED) {
        setState(() {
          estadoPermiso = false;
        });
      } else {
        setState(() {
          estadoPermiso = true;
        });
      }
    }
  }

  Future<List<MapBoxPlace>> search(String search) async {
    _locationData = await location.getLocation();

    print(_locationData);
    var placesService = PlacesSearch(
      apiKey: apiKey,
      country: "CL",
      limit: 5,
    );

    var places = await placesService.getPlaces(
      search,
      location: Location(
        lat: _locationData.latitude,
        lng: _locationData.longitude,
      ),
    );
    return places;
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return (estadoServicio && estadoPermiso)
        ? Scaffold(
      body: SafeArea(
        child: SearchBar<MapBoxPlace>(
          searchBarPadding: EdgeInsets.symmetric(horizontal: 10),
          headerPadding: EdgeInsets.symmetric(horizontal: 10),
          listPadding: EdgeInsets.symmetric(horizontal: 10),
          onSearch: search,
          searchBarController: _searchBarController,
          placeHolder: Center(
            child: Container(
              height: mediaQueryData.height,
              width: mediaQueryData.width,
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Image.asset(
                    "assets/Imagenes/icon_estacionateenchillan.png",
                    width: mediaQueryData.width * 0.7,
                  ),
                  Container(
                    width: mediaQueryData.width * .6,
                    child: Column(
                      children: <Widget>[
                        ButtonTheme(
                          minWidth: mediaQueryData.width * 0.6,
                          child: new RaisedButton(
                            shape: new RoundedRectangleBorder(
                                borderRadius:
                                new BorderRadius.circular(0.0),
                                side: BorderSide(
                                    color: Colors.transparent)),
                            padding: const EdgeInsets.all(8.0),
                            elevation: 0,
                            textColor: Colors.black,
                            color: Colors.white,
                            onPressed: () {
                              //TODO: Agregar acción
                            },
                            child: new Text("Busca el a donde quieres ir",
                                style:
                                TextStyle(fontFamily: "SenRegular")),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          cancellationWidget: Text("Cancelar"),
          emptyWidget: Center(
            child: Container(
              height: mediaQueryData.height,
              width: mediaQueryData.width,
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Image.asset(
                    "assets/Imagenes/icon_estacionateenchillan.png",
                    width: mediaQueryData.width * 0.7,
                  ),
                  Container(
                    width: mediaQueryData.width * .6,
                    child: Column(
                      children: <Widget>[
                        ButtonTheme(
                          minWidth: mediaQueryData.width * 0.6,
                          child: new RaisedButton(
                            shape: new RoundedRectangleBorder(
                                borderRadius:
                                new BorderRadius.circular(0.0),
                                side: BorderSide(
                                    color: Colors.transparent)),
                            padding: const EdgeInsets.all(8.0),
                            elevation: 0,
                            textColor: Colors.black,
                            color: Colors.white,
                            onPressed: () {
                              //TODO: Agregar acción
                            },
                            child: new Text("No encontramos destinos",
                                style:
                                TextStyle(fontFamily: "SenRegular")),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          onCancelled: () {
            print("Cancelled triggered");
          },
          onItemFound: (MapBoxPlace post, int index) {
            return Container(
              child: ListTile(
                leading: FaIcon(FontAwesomeIcons.mapMarkerAlt),
                title: Container(
                  width: mediaQueryData.width,
                  child: Text(
                    post.text,
                    style: TextStyle(fontSize: 12, fontFamily: "SenBold"),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) =>
                          DetailPage(
                            latitudDestino: post.geometry.coordinates[1],
                            longitudDestino: post.geometry.coordinates[0],
                            nombreDestino: post.text,
                            latitudUsuario: _locationData.latitude,
                            longitudUsuario: _locationData.longitude,)));
                },
              ),
            );
          },
        ),
      ),
    )
        : Scaffold(
      body: Center(
        child: Container(
          height: mediaQueryData.height,
          width: mediaQueryData.width,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Image.asset(
                "assets/Imagenes/icon_estacionateenchillan.png",
                width: mediaQueryData.width * 0.7,
              ),
              Container(
                width: mediaQueryData.width * .7,
                child: Column(
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: mediaQueryData.width * 0.7,
                      child: new RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(0.0),
                            side: BorderSide(color: Colors.black)),
                        padding: const EdgeInsets.all(8.0),
                        textColor: Colors.black,
                        color: Colors.white,
                        onPressed: () async {
                          await permisos();
                        },
                        child: Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Image.asset(
                              "assets/Imagenes/left-arrow.png",
                              width: 33,
                            ),
                            new Text("Dar Permisos de ubicación",
                                style:
                                TextStyle(fontFamily: "SenRegular")),
                          ],
                        ),
                      ),
                    ),
                    ButtonTheme(
                      minWidth: mediaQueryData.width * 0.6,
                      child: new RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(0.0),
                            side: BorderSide(color: Colors.transparent)),
                        padding: const EdgeInsets.all(8.0),
                        elevation: 0,
                        textColor: Colors.black,
                        color: Colors.white,
                        onPressed: () {
                          //TODO: Agregar acción
                        },
                        child: new Text(
                            "Necesitamos tus permisos de ubicación",
                            style: TextStyle(fontFamily: "SenRegular")),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
