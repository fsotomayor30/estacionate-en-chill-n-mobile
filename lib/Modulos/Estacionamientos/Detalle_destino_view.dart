import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:estacionate_en_chillan/Modulos/Entidades/Estacionamiento_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DetailPage extends StatefulWidget {
  final double latitudDestino;
  final double longitudDestino;
  final String nombreDestino;
  final double latitudUsuario;
  final double longitudUsuario;

  DetailPage(
      {this.latitudDestino,
      this.longitudDestino,
      this.nombreDestino,
      this.latitudUsuario,
      this.longitudUsuario});

  @override
  State<StatefulWidget> createState() {
    return DetailList();
  }
}

class DetailList extends State<DetailPage> {
  final databaseReference = Firestore.instance;
  List<Estacionamiento> estacionamientos = new List<Estacionamiento>();

  @override
  initState() {
    super.initState();
    cargarEstacionamientos();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void cargarEstacionamientos() {
    databaseReference
        .collection("Estacionamientos")
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      snapshot.documents.forEach((document) {
        estacionamientos.add(Estacionamiento(
            document['direccion'].toString(),
            document['esTechado'],
            document['habilitado'],
            document['id'],
            double.parse(document['latitud'].toString()),
            double.parse(document['longitud'].toString()),
            document['nombre'],
            int.parse(document['precioPorMinuto'].toString()),
            document['telefono'],
            document['tieneCamaraSeguridad']));
      });
      setState(() {
        print("Largo " + estacionamientos.length.toString());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text("Estacionamientos",
            style: TextStyle(fontFamily: "SenRegular")),
        backgroundColor: Color.fromRGBO(0, 0, 0, 1),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("Para ir a",
                    style: TextStyle(fontSize: 20, fontFamily: "SenRegular")),
                Text(
                  widget.nombreDestino,
                  style: TextStyle(fontSize: 30, fontFamily: "SenRegular"),
                ),
                Text("Tienes los siguientes estacionamientos",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20, fontFamily: "SenRegular")),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        child: Row(
                          children: <Widget>[
                            FaIcon(FontAwesomeIcons.sort),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Text("Precio"),
                            ),
                          ],
                        ),
                        onPressed: () {
                          //TODO: Agregar acción
                        },
                      ),
                      RaisedButton(
                        child: Row(
                          children: <Widget>[
                            FaIcon(FontAwesomeIcons.sort),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Text("Distancia"),
                            ),
                          ],
                        ),
                        onPressed: () {
                          //TODO: Agregar acción
                        },
                      ),
                    ],
                  ),
                ),
                //TODO: Terminar lógica
                Expanded(
                  child: ListView.builder(
                    itemCount: estacionamientos.length,
                    itemBuilder: (context, index) => ListTile(
                      title: Text(estacionamientos[index].nombre),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
