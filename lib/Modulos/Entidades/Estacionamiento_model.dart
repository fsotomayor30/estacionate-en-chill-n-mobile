class Estacionamiento {
  final String direccion;
  final bool esTechado;
  final bool habilitado;
  final String id;
  final double latitud;
  final double longitud;
  final String nombre;
  final int precioPorMinuto;
  final String telefono;
  final bool tieneCamaraSeguridad;

  const Estacionamiento(
      this.direccion,
      this.esTechado,
      this.habilitado,
      this.id,
      this.latitud,
      this.longitud,
      this.nombre,
      this.precioPorMinuto,
      this.telefono,
      this.tieneCamaraSeguridad);
}
