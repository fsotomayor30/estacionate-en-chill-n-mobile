import 'package:estacionate_en_chillan/Modulos/Entidades/Estacionamiento_model.dart';
import 'package:estacionate_en_chillan/Modulos/Inicio/Inicio_view.dart';
import 'package:estacionate_en_chillan/Modulos/Login/reducers/reducers.dart';
import 'package:estacionate_en_chillan/app_state.dart';
import 'package:estacionate_en_chillan/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:splashscreen/splashscreen.dart';

Future main() async {
  final _initialState = AppState(
      user: null,
      estacionamientos: List<Estacionamiento>()
  );
  final Store<AppState> _store =
  Store<AppState>(reducer, initialState: _initialState);

  runApp(new MaterialApp(
    home: new MyApp(store: _store),
  ));
}

class MyApp extends StatefulWidget {
  final Store<AppState> store;

  MyApp({this.store});

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return SplashScreen(
      seconds: 4,
      navigateAfterSeconds: new MyAppAfterSplash(
        store: widget.store,
      ),
      image: new Image.asset("assets/Imagenes/icon_estacionateenchillan.png",
          width: mediaQueryData.width * 0.7),
      backgroundColor: Colors.white,
      styleTextUnderTheLoader: new TextStyle(),
      loaderColor: Colors.transparent,
      loadingText: Text(
        "Estacionate donde tu quieras",
        style: TextStyle(
            color: Colors.black, fontSize: 20, fontFamily: "SenRegular"),
        textAlign: TextAlign.center,
      ),
      photoSize: 100.0,
    );
  }
}

class MyAppAfterSplash extends StatelessWidget {
  final appTitle = 'Estacionate en Chillán';
  final Store<AppState> store;

  MyAppAfterSplash({this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: InicioPage(),
        routes: buildAppRoutes(),
      ),
    );
  }
}
